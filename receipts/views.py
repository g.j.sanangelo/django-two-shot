from django.shortcuts import render , redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt , ExpenseCategory , Account
from receipts.forms import ReceiptForm , ExpenseCategoryForm , AccountForm


def receipt_list(request):
    receipt = Receipt.objects.all()
    context = {
        'receipt':receipt,
    }
    return render(request, 'receipts/home.html' , context)

def redirect_to_receipt_list(request):
    return redirect('home')


@login_required
def my_receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt": receipt,
    }
    return render(request, 'receipts/home.html', context)

@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
        context = {
            'form': form,
        }
        return render(request, 'receipts/create.html' , context)


@login_required
def category_list(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'expense': expense,
    }
    return render(request, 'receipts/categories.html', context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        'account': account,
    }
    return render(request, 'receipts/accounts.html', context)

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()
    context = {
            'form': form,
    }

    return render(request, 'receipts/create_category.html' , context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = ExpenseCategoryForm()
    context = {
            'form': form,
    }

    return render(request, 'receipts/create_account.html' , context)

#
